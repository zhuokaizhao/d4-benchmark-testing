import matplotlib.pyplot as plt

data_range = [10, 20, 30, 40, 50]

# d3 results
d3_unstru_uniquely = [0.039, 0.125, 0.487, 0.742, 1.132]
d3_unstru_duplicate = [0.086, 0.269, 0.637, 1.471, 2.822]
d3_unstru_divide = [0.124, 0.376, 0.919, 2.140, 4.006]

d3_poly_uniquely = [0.049, 0.124, 0.249, 0.578, 1.113]
d3_poly_duplicate = [0.064, 0.220, 0.643, 1.443, 2.780]
d3_poly_divide = [0.098, 0.387, 1.213, 2.169, 4.117]

# d4 results
d4_unstru_one = [0.093, 0.135, 0.354, 0.933, 1.384]
d4_unstru_all = [0.095, 0.202, 0.494, 0.882, 1.833]
d4_unstru_split = [0.290, 0.737, 1.855, 4.006, 7.585]

d4_poly_one = [0.089, 0.120, 0.330, 0.736, 1.453]
d4_poly_all = [0.062, 0.188, 0.434, 1.098, 1.815]
d4_poly_split = [0.341, 0.705, 1.776, 4.036, 7.844]

plt.figure(1)
plt.plot(data_range, d3_unstru_uniquely, label='d3_unstru_uniquely', color='magenta', linestyle='-')
plt.plot(data_range, d3_unstru_duplicate, label='d3_unstru_duplicate', color='magenta', linestyle='--')
plt.plot(data_range, d3_unstru_divide, label='d3_unstru_divide', color='magenta', linestyle=':')
plt.plot(data_range, d4_unstru_one, label='d4_unstru_one', color='cyan', linestyle='-')
plt.plot(data_range, d4_unstru_all, label='d4_unstru_all', color='cyan', linestyle='--')
plt.plot(data_range, d4_unstru_split, label='d4_unstru_split', color='cyan', linestyle=':')
plt.legend()
plt.title('D3 vs D4 with unstructured data')
plt.xlabel('data range (same for all x, y, z)')
plt.ylabel('seconds')

plt.figure(2)
plt.plot(data_range, d3_poly_uniquely, label='d3_ploy_uniquely', color='magenta', linestyle='-')
plt.plot(data_range, d3_poly_duplicate, label='d3_poly_duplicate', color='magenta', linestyle='--')
plt.plot(data_range, d3_poly_divide, label='d3_poly_divide', color='magenta', linestyle=':')
plt.plot(data_range, d4_poly_one, label='d4_poly_one', color='cyan', linestyle='-')
plt.plot(data_range, d4_poly_all, label='d4_poly_all', color='cyan', linestyle='--')
plt.plot(data_range, d4_poly_split, label='d4_poly_split', color='cyan', linestyle=':')
plt.legend()
plt.title('D3 vs D4 with polygon data')
plt.xlabel('data range (same for all x, y, z)')
plt.ylabel('seconds')
plt.show()

# plot d4 results
d4_times_10 = []
